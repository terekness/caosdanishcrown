package projectcaos; 

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.Timer;

public class Snippet {

	private static Snippet instance;
	private Date startDate = new Date();
	private Timer timer;
	private Random  rndNumber = new Random();   

	private Snippet() {
		//Singleton - is/must be private
	}

	/**
	 * @return the one and only instance of Snippet.
	 */
	public static Snippet getInstance () {
		if (instance == null) {
			instance = new Snippet ();
		}
		return instance;
	}

	/**
	 * Prints out the exact time this method is called with whatever is given in the parameter.<p>
	 * Uses {@link #dateToHHmmssString(Date)} on a new Date (which sets the time of the date to the time it is created) to exclude everything except the hours, minutes and seconds.<p>
	 * @param LogText any string, this is usually some kind of identifier to locate when or how many times something happened in a longer method.
	 */
	public void log (String LogText) {
		System.out.printf("%n" + dateToHHmmssString(new Date()) +" "+  LogText); 
	}
	
	/**
	 * Prints whatever is given in the parameter. Identical to the well known System.out.println() except that it makes a new line on the beginning of each print.
	 * @param LogText any string that has to be printed.
	 */
	public void log2 (String LogText) {
		System.out.printf("%n" +LogText); 
	}
	
	public void logSingleLine (String LogText) {
		System.out.printf(LogText); 
	}

	/**
	 * Print an informative message about an incomplete method.
	 */
	public void logMethodNotCompleted () {
		System.out.printf("%n" +dateToHHmmssString(new Date()) +" call to InComplete Method (fix it): " + Thread.currentThread().getStackTrace()[2]); 
	}
	
	/**
	 * Prints index 2 and 3 of the current threads stack dump.
	 */
	public void logShowMethodTrace () {
		System.out.printf("%n%s Method Call to: %s ", dateToHHmmssString(new Date()), Thread.currentThread().getStackTrace()[2]); 
		System.out.printf("%n%s Method Call to: %s ", dateToHHmmssString(new Date()), Thread.currentThread().getStackTrace()[3]);
	}

	
	/**
	 * Prints an error message with a time, stack dump and text.
	 * @param LogText any text that is to be printed with the stack dump and current time.
	 */
	public void logErrors (String LogText) {
		System.out.printf("%n%s method:%s %nERROR:%s", dateToHHmmssString(new Date()),Thread.currentThread().getStackTrace()[2] , LogText); 

		for (int i=0; i<Thread.currentThread().getStackTrace().length;i++) {
			log("Error Trace:" + i + " " + Thread.currentThread().getStackTrace()[i].toString());
		}
	}

	/**
	 * Randomizes a number between two variables.
	 * 
	 * @param from lowest possible number
	 * @param to highest possible number is (to-1).
	 * @return a random number between from and (to-1).
	 */
	public int getRandomInteger(int from, int to) {
		return rndNumber.nextInt(to-from) + from; 
	}

	
	/**
	 * Converts a Date to a String with the format HH:mm:ss.SSS
	 * 
	 * @param date any date
	 * @return a string made out of the date
	 */
	public  String dateToHHmmssString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss.SSS");
		return (ft.format(date));
	}

	/**
	 * @param date any date
	 * @param format the wanted format the returned String has to have (eg. "HH:mm:ss"
	 * @return a string made out of the date, but with the format that was given in the parameter
	 */
	public  String dateToCustomString (Date date, String format) {
		SimpleDateFormat ft = new SimpleDateFormat (format);
		return (ft.format(date));
	} 

	/**
	 * Converts a Date to a String using the format yyyy-MM-dd HH:mm.
	 * 
	 * @param date any date
	 * @return a string made out of the date
	 */
	public  String dateToSqlDateString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm");
		return (ft.format(date));
	}
	
	/**
	 * Converts a String that follows the format yyyy-MM-dd HH:mm to a Date.
	 * 
	 * @param s a string that follows the format yyyy-MM-dd HH:mm
	 * @return a date made out of the string
	 */
	public Date stringSqlFormatDate (String s)  {
		DateFormat formatter ; 
		Date date = null ;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			date = formatter.parse(s);
		} catch (ParseException e) {
			
			log("Your date String '" + s + " could not be parsed to this format 'yyyy-MM-dd HH:mm' -> method returned null (which is not what your are like to want). see  -> Snippet.stringyyyyMMddHHmmoDate (String s)");
			logShowMethodTrace();
		}
		return date;
	} 

	/**
	 * Convert a Date to a string.
	 * 
	 * @param myDate any date
	 * @return returns a string that follows the format yyyy-MM-dd
	 */
	public String dateToyyyyMMddString   (Date myDate ) {
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		return (ft.format(myDate));
	}

	/**
	 * Convert a Date to a string.
	 * 
	 * @param myDate any date
	 * @return returns a string that follows the format HH:mm
	 */
	public String dateToHHmmString   (Date myDate ) {
		String timeStr; 
		try {
			SimpleDateFormat ft = new SimpleDateFormat ("HH:mm");
			timeStr =  (ft.format(myDate));
		} catch (Exception e) {
			timeStr = "N/A"; // catch illegal Date incl null
		}
		return timeStr;
	}
	
	/**
	 * Convert a string that follows the format yyyy-MM-dd to a Date.
	 * @param s a string that follows the format yyyy-MM-dd
	 * @return a date
	 */
	public Date stringyyyyMMddToDate (String s) {
		DateFormat formatter ; 
		Date date = null ;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse(s);
		} catch (ParseException e) {
		}
		return date;
	} 
	
	/**
	 * Get the amount of seconds there is between two dates.
	 * 
	 * @param startDate earlier date
	 * @param endDate later date
	 * @return amount of seconds between startDate and endDate
	 */
	public  String getTimeDiff (Date startDate, Date endDate) {
		return (endDate.getTime() - startDate.getTime()) / 1000.0 + " sec";
	}

	/**
	// TODO
	 */
	public  String getTimeLapse () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 + " sec";
	}
	
	/**
	// TODO
	 */
	public  double getTimeLapseInSeconds () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 ;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartTimer() {
		this.startDate = new Date();
	}

	public String getTimeStampString() {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm");
		return myDateFormat.format(new Date());
	}
	public Date getTimeStampDate() {
		return new Date(); 
	}
	
	
	/**
	 * Changes a labels text, and then clears it after the timer runs out.
	 * 
	 * @param label the label which text is to be changed.
	 * @param message message the label will contain until getting cleared.
	 * @param waitTime how long the message will be shown, before getting cleared.
	 */
	public void timedMessage(final JLabel label, String message, int waitTime){
		label.setText(message);
		timer = new Timer(waitTime, new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				label.setText("");
			}
		});
		timer.setRepeats(false);
		timer.start();
	}

	/**
	 * Adds a specified amount of minutes to a given Date.
	 * 
	 * @param date the date that will have minutes added to it.
	 * @param minutes the amount of minutes that will be added to the date.
	 * @return a Date that has had its time increased by the amount of minutes added.
	 */
	public Date addMinutesToDate(Date date, int minutes)  {
		long ONE_MINUTE_IN_MILLIS=60000; 
		long t=date.getTime();
		date = new Date(t + (minutes * ONE_MINUTE_IN_MILLIS));
		return date;
	}
	
	public String readFile (String fileName) {
		FileInputStream filein = null;
		try {
			filein = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scanner scan = new Scanner(filein);

		String fileContext ="";
		while (scan.hasNext()) {
			String s=scan.nextLine();
			fileContext += s + "\n";
		}
		scan.close();
	    log("read '" +fileName + "' -> ok ");
	    log (fileContext);
	
		return fileContext;
	
	}
	
}