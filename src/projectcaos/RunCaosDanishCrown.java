package projectcaos;

import java.awt.BorderLayout;

import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.GridLayout;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;

/**
 * 
 * @version 0.15
 */
@SuppressWarnings("serial")
public class RunCaosDanishCrown extends JPanel {

	private static MsSqlServer db = MsSqlServer.getInstance();
	private static Snippet sn = Snippet.getInstance();

	private JFrame frame;
	private JPanel panel_North, panel_Center, panel_After;
	private JComboBox<String> cbQueryList;
	private JTextArea txaQuery, txaAfter;
	private JLabel lblAfter;
	private JScrollPane scrollQuery, scrollSql;


	private static String myServer   = Passwords.db_ipaddr;
	private static String myDatabase = "danishcrown";
	private static String myUserid   = Passwords.db_userid;
	private static String myPassword = Passwords.db_password;
	private static String myInstance =  Passwords.db_instance;
	private JPanel panel;
	private JButton btnRunQuery;
	private JPanel panel_1;
	private JTable table;

	public static void main(String[] args) {
		new RunCaosDanishCrown();
	}

	public RunCaosDanishCrown() {
		db.dbConnect(myServer, myDatabase, myUserid, myPassword,myInstance);
		frame = new JFrame();
		frame.setBounds(100, 100, 992, 694);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Rollback");
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		if (db.getRandomId(db.executeSqlQueryText("select * from LoadingOrder")) == -1) {
			CreateData createData = new CreateData();

			db.executeSql("delete  SALESORDER");
			db.executeSql("delete  LOADINGORDER");
			createData.createTables();

			sn.log("New Data Created in " + db.dbCurrentDatabase());
		}

		panel_North = new JPanel();
		frame.getContentPane().add(panel_North, BorderLayout.NORTH);
		panel_North.setLayout(new BorderLayout(0, 0));

		panel_1 = new JPanel();
		panel_North.add(panel_1, BorderLayout.WEST);

		cbQueryList = new JComboBox<String>();
		cbQueryList.setMaximumRowCount(15);
		panel_1.add(cbQueryList);
		cbQueryList.setSelectedItem(null);
		cbQueryList.addItem("Opg 2C");
		cbQueryList.addItem("Opg 3A");
		cbQueryList.addItem("Opg 3B");
		cbQueryList.addItem("Opg 3C");
		cbQueryList.addItem("Opg 4");
		cbQueryList.addItem("Opg 5 - Trigger");
		cbQueryList.addItem("LoadingOrder");
		cbQueryList.addItem("ErrorLog");
		cbQueryList.addItem("LateArrivels");
		cbQueryList.addItem("SP_DeleteData (5a)");
		cbQueryList.addItem("SQL Script");
		cbQueryList.setSelectedIndex(0);

		btnRunQuery = new JButton("Run Query");
		panel_1.add(btnRunQuery);
		btnRunQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
 
				if (txaQuery.getText().toUpperCase().contains("SELECT")) {
				    	txaAfter.setText(db.printData(db.executeSqlQueryText(txaQuery.getText()))+"");
					}
					else 					{
						db.executeSql(txaQuery.getText());
					}
			}	
		});
		btnRunQuery.setEnabled(false);

		table = new JTable();
		panel_North.add(table, BorderLayout.CENTER);
		cbQueryList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRunQuery.setEnabled(true);
				String selectedQuery = (String) cbQueryList.getSelectedItem();
				if(selectedQuery.equals("Opg 2C")) {
					txaQuery.setText("SELECT LO.ID as LOADINGORDER_ID, LO.DATEARRIVED, LO.DATECOMPLETEDLOADING, TR.LICENSEPLATE, TD.NAME as TRUCK_DRIVER, DO.DOCKSTATUS, DO.PACKINGTYPE, DO.ID as DOCK_ID"
							+ "\nFROM LOADINGORDER LO"
							+ "\nJOIN TRUCK TR on TR.FK_LOADINGORDER_ID = LO.ID " 
							+ "\nJOIN TRUCKDRIVER TD on TD.FK_TRUCK_ID = TR.ID "
							+ "\nJOIN DOCK DO on DO.ID = LO.DOCK_ID "
							+ "\nWHERE datediff(dd, LO.DATEARRIVED, '20140529') = 0 ");
				}
				if(selectedQuery.equals("Opg 3A")) {
					txaQuery.setText("SELECT count(*) antal, packingtype from DOCK d group by packingtype");
				}
				if(selectedQuery.equals("Opg 3B")) {
					txaQuery.setText("SELECT d.id, d.dockStatus, d.packingType, lo.DateArrived, lo.DateCompletedLoading as actualCompl, lo.DateExpectedCompletedLoading as expectedCompl, datediff (minute, DateCompletedLoading, DateExpectedCompletedLoading) as delayed"
							+ "\nFROM DOCK d"
							+ "\nJOIN LOADINGORDER lo on lo.DOCK_ID = d.ID"
							+ "\nWHERE datediff (minute, DateCompletedLoading, DateExpectedCompletedLoading) > 0"
							+ "\nORDER by d.PACKINGTYPE");
				}
				if(selectedQuery.equals("Opg 3C")) {

					txaQuery.setText(
							"select packingType, delayed, antal, (cast(delayed as decimal)/antal) as average " 
									+ " from  (select d.packingType, \n"
									+ " sum (datediff (minute, lo.DateCompletedLoading, lo.DateExpectedCompletedLoading))  as   delayed ,\n "
									+ " count(*) Antal \n"
									+ " from   DOCK d \n"
									+ " join   LOADINGORDER lo on lo.DOCK_ID = d.ID \n"
									+ " where  datediff (minute, DateCompletedLoading, DateExpectedCompletedLoading)  > 0 \n"
									+ " group by d.PACKINGTYPE) as temp \n");

				}
				if(selectedQuery.equals("Opg 4")) {
					txaQuery.setText("create Procedure SP_DELETEDATA"
							+ "\n\t@deleteFromDate datetime"
							+ "\nas"
							+ "\n\tset nocount on"
							+ "\n\tdelete from LOADINGORDER where DATEARRIVED < @deleteFromDate"
							+ "\n\tdelete from SALESORDER where LOADINGDATE   < @deleteFromDate"
							+ "\ngo");
					btnRunQuery.setEnabled(false);
				}
				if(selectedQuery.equals("SQL Script")) {
					txaQuery.setText( sn.readFile("src/files/create-msSQL_v3.sql")); 
					btnRunQuery.setEnabled(false);
				}
				
				if(selectedQuery.equals("Opg 5 - Trigger")) {
					txaQuery.setText( sn.readFile("src/files/trigger.sql")); 
					btnRunQuery.setEnabled(false);
				}
				if(selectedQuery.equals("LoadingOrder")) {
					txaQuery.setText("SELECT ID, DATEARRIVED,  DATECOMPLETEDLOADING, " 
						+ " DATEEXPECTEDCOMPLETEDLOADING ,DOCK_ID  FROM LOADINGORDER");
    			}
				if(selectedQuery.equals("ErrorLog")) {
					txaQuery.setText("SELECT * from ErrorLog");
    			}
				
				if(selectedQuery.equals("LateArrivels")) {
					txaQuery.setText("Select ID, DATEARRIVED, DATEEXPECTEDCOMPLETEDLOADING AS EXPECTED_LOAD_COMP, DATECOMPLETEDLOADING, "  
						+ " DATEDIFF(MINUTE,DATEEXPECTEDCOMPLETEDLOADING,DATECOMPLETEDLOADING) AS DELAY FROM loadingOrder"
						+ " WHERE DATEDIFF(MINUTE,DATEEXPECTEDCOMPLETEDLOADING,DATECOMPLETEDLOADING) >= 15 ");
				}
				
				if(selectedQuery.equals("SP_DeleteData (5a)")) {
					txaQuery.setText("{call SP_DELETEDATA ('2014-05-30 00:00')}");
				}
			}
		});

		panel_Center = new JPanel();
		frame.getContentPane().add(panel_Center, BorderLayout.CENTER);
		panel_Center.setLayout(new GridLayout(2, 1, 0, 0));

		panel = new JPanel();
		panel_Center.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		txaQuery = new JTextArea();
		txaQuery.setFont(new Font("Courier New", Font.BOLD, 13));
		scrollQuery = new JScrollPane(txaQuery);
		scrollQuery.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollQuery.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel.add(scrollQuery);
		txaQuery.setEditable(true);

		panel_After = new JPanel();
		panel_Center.add(panel_After);
		panel_After.setLayout(new BorderLayout(0, 0));

		lblAfter = new JLabel("SQL RESULT");
		lblAfter.setHorizontalAlignment(SwingConstants.CENTER);
		panel_After.add(lblAfter, BorderLayout.NORTH);

		txaAfter = new JTextArea();
		scrollSql = new JScrollPane(txaAfter);
		panel_After.add(scrollSql);
		txaAfter.setFont(new Font("Courier New", Font.BOLD, 13));
		txaAfter.setEditable(false);
		frame.setVisible(true);
	}

	public void allMethods() {
		Date startDate = new Date();

		Date startDateExclConnect = new Date();

		// check if LoadingOrders Exists, and if not create data (opgave 1)
		if (db.getRandomId(db.executeSqlQueryText("select * from LoadingOrder")) == -1) {
			CreateData createData = new CreateData();

			db.executeSql("delete  SALESORDER");
			db.executeSql("delete  LOADINGORDER");
			createData.createTables();

			sn.log("New Data Created in " + db.dbCurrentDatabase());
		}

		db.printData(db.executeSqlQueryText("select * from errorlog"));
		db.printData(db.executeSqlQueryText("select * from Customer"));
		db.printData(db.executeSqlQueryText("select * from Trailer"));
		db.printData(db.executeSqlQueryText("select * from TruckDriver"));

		db.printData(db.executeSqlQueryText("select * from SubOrder") );
 
		//ref 5
		

		sn.log("Inserts/Queries Completed in " + sn.getTimeDiff(startDateExclConnect, new Date()));

		db.dbDisconnect();
		sn.log("All Completed in " + sn.getTimeDiff(startDate, new Date()));
	}

}
