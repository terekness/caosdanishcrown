package projectcaos;  

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Perform common tasks on a MS SQL Database
 * 
 * @version 0.06
 * @author  Lars Klingsten
 *
 */
public class MsSqlServer {
	private static     MsSqlServer instance;
	private Connection msConn;
	private Snippet    sn = Snippet.getInstance();
	private String     serverIpAddr = "No Connections attempted";
	private String     database     = "No Connections attempted";

	public static MsSqlServer getInstance () {
		if (instance == null) {
			instance = new MsSqlServer ();
		}
		return instance; 
	}

	private MsSqlServer() {
		//singleton pattern (=private constructor)
	}

	/**
	 * Connects to a MS SqlServer
	 * 
	 * @param serverIpAddr   (IP address) for MsSQLServer
	 * @param database
	 * @param db_userid      user id 
	 * @param db_password    user Password
	 * @param instance       name for msSqlServer
	 */
	public  void dbConnect(String serverIpAddr, String database,  String db_userid, String db_password, String instance) {
		try
		{
			this.serverIpAddr = serverIpAddr;
			this.database = database;

			String db_connect_string =  String.format("jdbc:jtds:sqlserver://%s:1433/%s;instance=%s",serverIpAddr, database, instance );
			Class.forName("net.sourceforge.jtds.jdbc.Driver"); // Microsoft SQL-Server
			if ( msConn == null  || msConn.isClosed()) {
				msConn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
			}
			sn.log("connnected to server: " +  serverIpAddr + " db: " +  database);
		}
		catch (Exception e)	{
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return the current database that is connected
	 */
	public String dbCurrentDatabase () {
		String currentDatabase= "" ; 
		try {
			currentDatabase =  msConn.getCatalog().toString();
		} catch (SQLException e) {
			sn.logErrors(" Check if you are connected ... ");;
		}	
		return currentDatabase ;
	}

	/**
	 * Disconnect from the Database
	 */
	public void dbDisconnect() {
		try {
			msConn.close();
			sn.log("disconnected from server: " + this.serverIpAddr+ " db: " +this.database);
		} catch (SQLException e) {
			e.printStackTrace();
		};
	}

	/**
	 * Converts Java Objects (String, Int, Date, Null) into preparred SQLString using (?,?,?) style) 
	 * 
	 * @param preparedStatement 
	 * @param value that needs to be converted
	 * @param index Please that these indexes starts with 1 (not 0)
	 */
	private void dbSetValueToPreparedStatement (PreparedStatement preparedStatement, Object value, int index) {
		try {
			if (value instanceof String)   {
				preparedStatement.setString  (index , (String)value);
			}
			else if (value instanceof Integer)   {
				preparedStatement.setInt(  index , (int)value);
			}
			else if (value instanceof Date)  {

				preparedStatement.setString( index, sn.dateToSqlDateString((Date)value));
			}
			else if (value == null)   {
				sn.log("null" );
				preparedStatement.setNull(  index, java.sql.Types.NULL);
			}

			else {
				sn.logErrors("This type can not be converted" + value.toString());
			}
		}
		catch (SQLException e) {
			sn.logErrors("convertion error");
		}
	}

	/**
	 * Insert a new record/row into a database using database column names, and values
	 * @link msSqlServer.
	 * 
	 * usage:String[] columnsSalesOrder = {"DELIVERYADDRESS","DELIVERYCOUNTRY",	"LOADINGDATE","LOADINGLOCATION","FK_CUSTOMER_ID"};
	 *		 Object[] valuesSalesOrder = { "Blichers Gade 12", 	"Holland", 	startDate,  "Horsens",rndCustomer};
	 *		 int salesOrderID = db.dbInsertRow("SALESORDER", columnsSalesOrder, valuesSalesOrder);
	 * 
	 * @param table Name
	 * @param columns
	 * @param values
	 */
	public int dbInsertRow (String table,String [] columns, Object [] values ) {
		String sqlColumns ="";
		@SuppressWarnings("unused")
		String sqlValues =""; 
		String sqlString;
		int dbGeneratoedID = -1;

		int colCount =  values.length-1; 
		String tableQuestionsMarks = "";

		for (int i=0; i <=  colCount; i++) {
			sqlColumns += (""+columns[i]  + ",");
			sqlValues  += ("'"+values[i]  + "',");  
			tableQuestionsMarks += "?,"; // // create (?,? ... ?) to match number of columns
		}

		sqlColumns = sqlColumns.substring(0,sqlColumns.length()-1);
		tableQuestionsMarks = tableQuestionsMarks.substring(0,tableQuestionsMarks.length()-1);

		sqlString = String.format ("insert into %s (%s) values (%s)", table, sqlColumns, tableQuestionsMarks) ;

		try {	
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString,Statement.RETURN_GENERATED_KEYS);
			for (int i=0; i <=  colCount; i++) {
				dbSetValueToPreparedStatement(preparedStatement, values[i], i+1);
			}

			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			rs.next();
			dbGeneratoedID = rs.getInt("id");
			sn.log("Inserted 1 record into table " + table + " with id: " + dbGeneratoedID);

		} catch (SQLException e) {
			sn.logErrors(e.getMessage());
		}
		return dbGeneratoedID;
	}

	/**
	 * Quick inserts in database table, without the the usage of columns, 
	 * however value order must match Database structure (ofcause)
	 * @param table names
	 * @param values - their order much match the DB structure
	 */
	public int dbInsertRowWithoutColumns (String table, Object [] values ) {
		@SuppressWarnings("unused")
		String sqlValues =""; 
		String sqlString;
		int colCount =  values.length-1;
		int dbGeneratoedID = -1;
		String tableQuestionsMarks = "";

		for (int i=0; i <=  colCount; i++) {
			sqlValues  += ("'"+values[i]  + "',");  
			tableQuestionsMarks += "?,"; // // create (?,? ... ?) to match number of columns
		}
		tableQuestionsMarks = tableQuestionsMarks.substring(0,tableQuestionsMarks.length()-1);
		sqlString           = String.format ("insert into %s values (%s)", table,tableQuestionsMarks) ;

		try {	
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString,Statement.RETURN_GENERATED_KEYS);
			for (int i=0; i <=  colCount; i++) {
				dbSetValueToPreparedStatement(preparedStatement, values[i], i+1);
			}
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			rs.next();
			dbGeneratoedID = rs.getInt("id");
			sn.log("Inserted 1 record into table " + table + " with id: " + dbGeneratoedID);
		} catch (SQLException e) {
			sn.logErrors(e.getMessage());
		}
		return dbGeneratoedID;
	}

	/**
	 * Updates a single row in table
	 * @param table
	 * @param column
	 * @param value
	 * @param condition
	 */
	public void dbUpdateRow (String table, String column, Object value, String condition) {
		String sqlString = String.format ("update %s set  %s = ?  where %s", table, column, condition) ;
		try {
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString);
			dbSetValueToPreparedStatement(preparedStatement, value, 1);
			sn.log("updated: "+ sqlString +  " (? = " + value + ")");

		} catch (SQLException e) {
			sn.logErrors("Update did not happen! Error:" + e.getMessage() );
		}
	}

	/**
	 * execute a SQL statement, and return result as a resultSet
	 * @param sqlStatement
	 * @return ResultSet
	 */
	public ResultSet executeSqlQueryText (String sqlStatement)   { 
		ResultSet rs = null;
		try {
			sn.setStartTimer();
			String sqlString = String.format (sqlStatement) ;
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString);
			rs = preparedStatement.executeQuery();
		} catch (SQLException e) {
			sn.logErrors("Error: "+e.getMessage());
		}
		return rs;
	}

	/**
	 * Get a random record ID. Note table must have "ID" as its primary key
	 * 
	 * usage sample: int id = db.getRandomId(db.executeSqlQueryText("select * from Trailer"))
	 * 
	 * @param rs ResultSet 
	 * @return Integer with the ID value of the ResultSet (or -1 there is no data or column name != "id"
	 */
	public int getRandomId(ResultSet rs )   {
		int returnvalue = -1;
		List <Integer> randumNumbers = new ArrayList <Integer>();
		try {
			while (rs.next()) {
				randumNumbers.add((int) rs.getObject("id"));
			}
			
			if (randumNumbers.size() > 0) {
				returnvalue = randumNumbers.get(sn.getRandomInteger(0, randumNumbers.size()));
			} 

		} catch (SQLException e) { } // sql exceptions }
		return returnvalue;
	}

/**
 * Execute updates, and other commands on MsSqlServer
 * 
 * usage: db.executeSql("Select * from errorLog")
 * 
 * @param sqlStatement
 */
	public void executeSql (String sqlStatement)   { 
		try {
			String sqlString = String.format (sqlStatement) ;
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			sn.logErrors("Error: "+e.getMessage());
		}
	}

	/**
	 * Deletes some old data
	 * @param date
	 */
	public void sp_DeleteData (Date date) {
		executeSql(String.format ("{call SP_DELETEDATA ('%s')}", sn.dateToCustomString(date, "yyMMdd")) );
	}

	/**
	 * prints table data to log/console
	 * 
	 * usage: db.printData(db.executeSqlQueryText("select * from <table>"));
	 * 
	 * @param rs ResultSet
	 */
	public String printData(ResultSet rs  )  {
		int countRows = 0;
		int colCount;
		String resultString = "\n";
 
		// print colunms
		try {
			colCount = rs.getMetaData().getColumnCount();
			for (int i=1; i <=  colCount; i++) {
				
				resultString += String.format("| %1$-23s " ,rs.getMetaData().getColumnName(i) )+ " ";
			}

			//print data
			resultString+="\n";
			while (rs.next()) {
				countRows++;
				for (int i=1; i <=  colCount; i++) {
					resultString += String.format("| %1$-23s " , rs.getObject(i)) + " ";
					  
				}
				resultString+="\n";
			}
			resultString += "-------\n";
			resultString += "rows:" + countRows + " in " + sn.getTimeLapse()+"\n" ;

		} catch (SQLException e) {
			sn.logErrors("Error: "+ e.getMessage());
		}
		
		sn.log2(resultString);
		return resultString;
	}
}