package projectcaos;  

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JTextArea;

import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

/**
 * 
 * @author Tarek Orfali
 * @version 0.05
 */
@SuppressWarnings("serial")
public class RollBack extends JPanel {
	private JFrame frame;
	private JPanel panel_All, panel_Buttons;
	private JTextArea txaTrans;
	private JButton btnFirstTrans, btnContinueTrans, btnCloseConnection;
	private JScrollPane txaTransScroll;
	private Controller btnCtrl;
	
	private Snippet sn = Snippet.getInstance();
  	private MsSqlServer db = MsSqlServer.getInstance();

	private static String myServer     = Passwords.db_ipaddr;
	private static String myDatabase   = "danishcrown";
	private static String myUserid     = Passwords.db_userid;
	private static String myPassword   = Passwords.db_password;
	private static String myInstance   =  Passwords.db_instance;
	boolean transIsActive, mayContinue = false;
	private int trailerID;

	private Connection msConn;
	private static Statement stmt;
	private JLabel lblInfoMsg;

	public static void main(String[] args) {
		new RollBack();
	} 

	public RollBack() {
		initGUI();
		this.frame.setVisible(true);
	}

	public void initGUI() {
		frame = new JFrame();
		frame.setBounds(100, 100, 774, 560);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Rollback");
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		btnCtrl = new Controller();

		panel_All = new JPanel();
		frame.getContentPane().add(panel_All, BorderLayout.CENTER);
		panel_All.setLayout(new BorderLayout(0, 0));

		txaTrans = new JTextArea();
		txaTransScroll = new JScrollPane(txaTrans);
		panel_All.add(txaTransScroll);
		txaTrans.setEditable(false);

		panel_Buttons = new JPanel();
		panel_All.add(panel_Buttons, BorderLayout.EAST);

		btnCloseConnection = new JButton("Close connection manually");
		btnCloseConnection.addActionListener(btnCtrl);
		panel_Buttons.setLayout(new GridLayout(4, 1, 0, 10));
				
		lblInfoMsg = new JLabel("<html>1) \r\nRun two instances<br>2) Press Start on both instances<br>3) Press Continue on both instances</html>");
		panel_Buttons.add(lblInfoMsg);
		
		btnFirstTrans = new JButton("Start transaction");
		panel_Buttons.add(btnFirstTrans);
		btnFirstTrans.addActionListener(btnCtrl);
		
		btnContinueTrans = new JButton("Continue transaction");
		panel_Buttons.add(btnContinueTrans);
		btnContinueTrans.addActionListener(btnCtrl);
		panel_Buttons.add(btnCloseConnection);

	}
 
	
	public void startTransaction() {
		try {
			String db_connect_string =  String.format("jdbc:jtds:sqlserver://%s:1433/%s;instance=%s",myServer, myDatabase, myInstance );
			Class.forName("net.sourceforge.jtds.jdbc.Driver"); // Microsoft SQL-Server
			if ( msConn == null  || msConn.isClosed()) {
				msConn = DriverManager.getConnection(db_connect_string, myUserid, myPassword);
			}

			stmt = msConn.createStatement();
			txaTrans.setText("connnected to server: " +  myServer + " db: " +  myDatabase);
	
			// new
			 stmt.execute("update trailer set  dock_ID = null  where dock_ID = 2 ");

			msConn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
			msConn.setAutoCommit(false); // starter en transaktion; SKAL afsluttes med commit
			transIsActive = true;

			trailerID = db.getRandomId(executeSqlQueryText("select * from Trailer"));
			
			String sqlText = "select COUNT(*) rows from DOCK d left join trailer t on t.DOCK_ID = d.ID " 
						+ "  where t.DOCK_ID = 2  OR (t.DOCK_ID is NOT NULL and t.id = " +trailerID +")";
			
			System.out.println(sqlText);
			
			ResultSet res = executeSqlQueryText(sqlText);

			if(res.next()) {
				if(res.getInt(1) == 0 && trailerID != -1) {
					txaTrans.append("\nNo rows returned\nYou may continue");
					mayContinue = true;
				}
				else {
					txaTrans.append("\n" + res.getInt(1) + " rows returned");
					txaTrans.append("\nYou may not continue. Please make sure no TRAILER already has DOCK_ID = 2, or is otherwise accupid by another Trailer");
				}
			}

		}
		catch (Exception e)	{
			e.printStackTrace();
			disconnect();
		}
	}
	
	public void continueTransaction() {
		if(mayContinue) {
			txaTrans.append("\nContinuing");

			try {
				stmt.execute(String.format("UPDATE TRAILER SET DOCK_ID = 2 WHERE ID = %s", trailerID));
				txaTrans.append("\nSQL query done");
				msConn.commit();
				disconnect();
			}
			catch (SQLException e) {
				txaTrans.append("\nA deadlock has occurred.\n" + e.getMessage());
			}
		}
		else {
			txaTrans.append("\nYou may not continue");
		}
	}
	
	public void disconnect() {
		try {
			msConn.close();
			sn.log("disconnected from server" );
		} catch (SQLException e) {
			e.printStackTrace();
		};
	}
	
	public ResultSet executeSqlQueryText (String sqlStatement)   { 
		ResultSet rs = null;
		try {
			sn.setStartTimer();
			String sqlString = String.format (sqlStatement) ;
			PreparedStatement preparedStatement = msConn.prepareStatement(sqlString);
			rs = preparedStatement.executeQuery();
		} catch (SQLException e) {
			sn.logErrors("Error: "+e.getMessage());
		}
		return rs;
	}

	public class Controller implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton source = (JButton) e.getSource();
			if(source.equals(btnFirstTrans)) {
				startTransaction();
			}
			if(source.equals(btnContinueTrans)) {
				continueTransaction();
			}
			if(source.equals(btnCloseConnection)) {
				disconnect();
				txaTrans.append("\nConnection closed");
			}

		}
	}
}
