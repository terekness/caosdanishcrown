package projectcaos;

import java.util.Date;
import java.util.Random;

/**
 * 
 *@author Tarek Orfali
 *@version 0.05
 */
public class CreateData {

	private MsSqlServer db = MsSqlServer.getInstance();
	private Random rnd = new Random();
	private Snippet sn = Snippet.getInstance();


	public void createDataAll() {
	}

	public void createTables() {

		Date startDate = sn.stringSqlFormatDate("2014-05-29 07:00");
		
		for (int k = 0;  k< 20; k++) {
			int rndCustomer = db.getRandomId(db.executeSqlQueryText("select id from customer"));
			sn.log("rndCustomer"+rndCustomer);
 
			//SalesOrders
			String[] columnsSalesOrder = {"DELIVERYADDRESS", 	"DELIVERYCOUNTRY", 		"LOADINGDATE", 	"LOADINGLOCATION", 	"FK_CUSTOMER_ID"};
			Object[] valuesSalesOrder = { "Blichers Gade 12", 	"Holland", 				startDate,       "Horsens", 		rndCustomer};
			int salesOrderID = db.dbInsertRow("SALESORDER", columnsSalesOrder, valuesSalesOrder);

			
			// LoadingOrders
			String[] columnsLoadingOrder = {"DATEARRIVED", "DATEEXPECTEDCOMPLETEDLOADING", "DATECOMPLETEDLOADING", "DOCK_ID"};
			Object[] valuesLoadingOrder = { sn.addMinutesToDate(startDate, 
											sn.getRandomInteger(-500,20)), 
					                        startDate,sn.addMinutesToDate(startDate, sn.getRandomInteger(-25,25)),
					                        db.getRandomId(db.executeSqlQueryText("select * from Dock"))};
			int loadingOrderID = db.dbInsertRow("LOADINGORDER", columnsLoadingOrder, valuesLoadingOrder);

			// suborders
			for(int i = 0; i < 3; i++) {
				String[] columns = {"PACKINGTYPE", 			"WEIGHTKG", 						"FK_SALESORDER_ID", "FK_LOADINGORDER_ID"};
				Object[] values = {getRandomPackingtype(),  sn.getRandomInteger(5000, 7000), 	salesOrderID, 		loadingOrderID};
				db.dbInsertRow("SUBORDER", columns, values);
			}

			// Trucks
			String[] columnsTruck = {"LICENSEPLATE", 		             	"FK_LOADINGORDER_ID", 	"FK_TRUCKINGCOMPANY_ID"};
			Object[] valuesTruck  = {"XK"+sn.getRandomInteger(40000, 49999), loadingOrderID, 		1};
			int truckID = db.dbInsertRow("Truck", columnsTruck, valuesTruck);
			
			// Trailers
			String[] columnTrailer = {"LICENSEPLATE",							"PACKINGTYPE",				"FK_TRUCK_ID"};
			Object[] valuesTrailer = {"TR"+sn.getRandomInteger(4000, 4999),	getRandomPackingtype(), 	truckID};
			int trailerID = db.dbInsertRow("TRAILER", columnTrailer, valuesTrailer);
			
			// TruckDrivers
//			String[] columnTruckDrivers = {"LICENSEPLATE",							"PACKINGTYPE",				"FK_TRUCK_ID"};
			Object[] valuesTruckDrivers = {sn.getRandomInteger(40000000,50000000),	"Lars"+trailerID, 	truckID};
			int TruckDriverID = db.dbInsertRowWithoutColumns("TruckDriver",  valuesTruckDrivers);
			
			startDate = sn.addMinutesToDate(startDate, 120);
		}
		
		
		// set Random Trailer at Dock lk 0.05
		int rndTruck = db.getRandomId(db.executeSqlQueryText("select id from truck"));
		db.executeSql("update Trailer set DOCK_ID = 1 where id =  " + rndTruck );
		rndTruck = db.getRandomId(db.executeSqlQueryText("select id from truck"));
		db.executeSql("update Trailer set DOCK_ID = 4 where id =  " + rndTruck );
		rndTruck = db.getRandomId(db.executeSqlQueryText("select id from truck"));
		db.executeSql("update Trailer set DOCK_ID = 5 where id =  " + rndTruck );
		
	}

//	 
	
	public String getRandomPackingtype() {
		int tal = rnd.nextInt(3) + 1;
		if(tal == 1) {
			return "BOX";
		}
		else if(tal == 2) {
			return "TUB";
		}
		else {
			return "XTREE";
		}
	}
}
