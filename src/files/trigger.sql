﻿-- version 3.13  
 
use danishcrown            
go
 
IF OBJECT_ID ('CheckDelays','TR') IS NOT NULL
    DROP TRIGGER CheckDelays;
GO

CREATE TRIGGER CheckDelays ON [LOADINGORDER] 
after UPDATE, INSERT, DELETE
as    
declare
      @MINUTES integer, 
      @ID integer, 
      @DATECOMPLETEDLOADING  datetime, 
      @DATEEXPECTEDCOMPLETEDLOADING datetime 
    
BEGIN 
  SELECT @DATECOMPLETEDLOADING = DATECOMPLETEDLOADING , 
         @DATEEXPECTEDCOMPLETEDLOADING = DATEEXPECTEDCOMPLETEDLOADING,
         @ID = ID, 
         @minutes = ( datediff (MINUTE, @DATEEXPECTEDCOMPLETEDLOADING, @DATECOMPLETEDLOADING ) )
   FROM  inserted;
IF ( @minutes >=   15 )
begin
       if exists(SELECT * from inserted) and exists (SELECT * from deleted)
       begin 
      	 INSERT INTO ERRORLOG (ERRORMESSAGE)  
		 VALUES   (CONCAT ('UPDATE: LOADING ORDER: ' , @ID, ' DELAYED FOR ', @minutes, ' MINUTES')  ) 
       end

       If exists (Select * from inserted) and not exists(Select * from deleted)
       begin
         INSERT INTO ERRORLOG (ERRORMESSAGE)  
	            VALUES   (CONCAT ('INSERT: LOADING ORDER: ' , @ID, ' DELAYED FOR ', @minutes, ' MINUTES')  ) 
        end
end

If exists(select * from deleted) and not exists(Select * from inserted)
begin 
    SELECT    @ID = ID   
	    FROM  deleted;

	INSERT INTO ERRORLOG (ERRORMESSAGE)  
	VALUES   (CONCAT ('DELETE: LOADING ORDER: ' , @ID)  ) 
end
END

GO
