﻿-- version 3.13  
 
if db_id('danishcrown') is null
create database danishcrown
go
 
use danishcrown            
go

-- -----------------------------------------------------
-- Table `danishcrown`.`CUSTOMER` 
-- -----------------------------------------------------

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CUSTOMER]'))
BEGIN -- LK 0.01
CREATE TABLE CUSTOMER (
  ID           INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  NAME         VARCHAR(255) NULL DEFAULT NULL,
  ADDRESS1     VARCHAR(255) NULL DEFAULT NULL,
  COUNTRY      VARCHAR(255) NULL DEFAULT NULL,
  EMAILADDRESS VARCHAR(255) NULL DEFAULT NULL,
  PHONENUMBER  INT          NULL DEFAULT NULL)
 
  INSERT INTO CUSTOMER(NAME, ADDRESS1, COUNTRY, EMAILADDRESS, PHONENUMBER) 
         VALUES ('Tareks MeatPackers', 'Monalisa',     'Italy',   'idied1519@hotmail.com',  '70401000') 
  INSERT INTO CUSTOMER(NAME, ADDRESS1, COUNTRY, EMAILADDRESS, PHONENUMBER) 
         VALUES ('Jenses Top Burgers', 'Lollandsgade', 'Holland', 'topbetaltgg@hotmail.com','23231001')
  INSERT INTO CUSTOMER(NAME, ADDRESS1, COUNTRY, EMAILADDRESS, PHONENUMBER) 
         VALUES ('Lars Klingsten',     'Blichers Gade','Tyskland','BestLars@yahoo.dk',      '23231002')  
  END
-- -----------------------------------------------------
-- Table `danishcrown`.`TRUCKINGCOMPANY` LK
-- -----------------------------------------------------
 IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TRUCKINGCOMPANY]'))
 BEGIN -- LK 0.01
   CREATE TABLE TRUCKINGCOMPANY (
   ID            INT       NOT NULL IDENTITY(1,1) PRIMARY KEY,
   NAME          NVARCHAR(255) NULL DEFAULT 'N/A',
   ADDRESS1      NVARCHAR(255) NULL DEFAULT 'N/A',
   COUNTRY       NVARCHAR(255) NULL DEFAULT 'N/A',
   EMAILADDRESS  NVARCHAR(255) NULL DEFAULT 'N/A',
   PHONENUMBER   INT           NULL DEFAULT -1)

   INSERT INTO TRUCKINGCOMPANY (NAME, ADDRESS1, COUNTRY, EMAILADDRESS, PHONENUMBER) 
        VALUES ('DSV Trucking', 'Tebakken 100, 8000 Aarhus C', 'Danmark', 'logistics@dsv.dk', '10203040')
 END 
  
-- -----------------------------------------------------
-- Table `danishcrown`.`LOADINGORDER`
-- -----------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[LOADINGORDER]'))

CREATE TABLE LOADINGORDER (
  ID                           INT  NOT NULL PRIMARY KEY IDENTITY(1,1),
  DATEARRIVED                  DATETIME NULL DEFAULT NULL,
  DATECOMMENCEDLOADING         DATETIME NULL DEFAULT NULL,
  DATECOMPLETEDLOADING         DATETIME NULL DEFAULT NULL,
  DATEDEPARTED                 DATETIME NULL DEFAULT NULL,
  DATEDRIVERRESTPERIODENDS     DATETIME NULL DEFAULT NULL,
  DATEEXPECTEDLOADING          DATETIME NULL DEFAULT NULL,
  DATEINSTRUCTEDARRIVALTIME    DATETIME NULL DEFAULT NULL,
  DATERELOAD                   DATETIME NULL DEFAULT NULL,
  DATEEXPECTEDCOMPLETEDLOADING DATETIME NULL DEFAULT NULL,
  DOCK_ID                      INT      NULL DEFAULT NULL)

------------------------------------------------------------------
-- Table 'danishcrown' . 'TRUCK'
------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TRUCK]'))


CREATE TABLE TRUCK(
	ID															INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	DATESIMULATIONTRUCKDRIVERARRIVE								DATETIME NULL,
	DATESIMULATIONTRUCKDRIVERDEPARTURE							DATETIME NULL,
	DATESIMULATIONTRUCKDRIVERRECEIVEDSMSLOADINGCOMPLETED		DATETIME NULL,
	LICENSEPLATE												NVARCHAR(255) NULL,
	LOGBOOK														NVARCHAR(MAX) NULL,
	QUEUENUMBER													INT NULL,
	TRAILERSTATUS												NVARCHAR(255) NULL,
	WEIGHTAFTERLOADKG											INT NULL,
	WEIGHTBEFORELOADKG											INT NULL,
	FK_LOADINGORDER_ID											INT NULL,
	FK_TRUCKINGCOMPANY_ID										INT NULL,
	FOREIGN KEY (FK_LOADINGORDER_ID)    REFERENCES [dbo].[loadingorder] ([ID])    ON DELETE CASCADE,
	FOREIGN KEY (FK_TRUCKINGCOMPANY_ID) REFERENCES [dbo].[truckingcompany] ([ID]) ON DELETE CASCADE
)
   
   -- -----------------------------------------------------
-- Table `danishcrown`.`SALESORDER`
-- -----------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[SALESORDER]'))
CREATE TABLE SALESORDER (

   ID               INT           NOT NULL PRIMARY KEY IDENTITY(1,1),
   DELIVERYADDRESS  NVARCHAR(255) NULL DEFAULT NULL,
   DELIVERYCOUNTRY  NVARCHAR(255) NULL DEFAULT NULL,
   LOADINGDATE      DATETIME      NULL DEFAULT NULL,
   LOADINGLOCATION  NVARCHAR(255) NULL DEFAULT NULL,
   FK_CUSTOMER_ID   INT           NULL DEFAULT NULL,
   FOREIGN KEY (FK_CUSTOMER_ID) REFERENCES [dbo].[CUSTOMER] ([ID]) ON DELETE CASCADE
   )
   
-- -----------------------------------------------------
-- Table `danishcrown`. TRAILER
-- -----------------------------------------------------

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TRAILER]'))
CREATE TABLE TRAILER (
  ID					INT           NOT NULL PRIMARY KEY IDENTITY(1,1),
  LICENSEPLATE			NVARCHAR(255) NULL DEFAULT NULL,
  PACKINGTYPE			NVARCHAR(255) NULL DEFAULT NULL,
  FK_TRUCK_ID			INT           NOT NULL,
  DOCK_ID               INT           NULL DEFAULT NULL -- LK 0.01 (NOT FK - BUT STILL POSSIBLE TO SET RELATIONSHIP
  FOREIGN KEY ([FK_TRUCK_id]) REFERENCES [dbo].[TRUCK] ([id]) ON DELETE CASCADE
  )

-- -----------------------------------------------------
-- Table danishcrown.`SUBORDER`
-- -----------------------------------------------------

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[SUBORDER]'))

CREATE TABLE SUBORDER(
  ID					INT           NOT NULL PRIMARY KEY IDENTITY(1,1),
  PACKINGTYPE			NVARCHAR(255) NULL DEFAULT NULL,
  WEIGHTKG				INT	          NULL DEFAULT NULL,
  FK_SALESORDER_ID   	INT           NULL DEFAULT NULL,
  FK_LOADINGORDER_ID	INT           NULL DEFAULT NULL,
  FOREIGN KEY ([FK_LOADINGORDER_id])  REFERENCES [dbo].[LOADINGORDER] ([id]) ON DELETE CASCADE,
  FOREIGN KEY ([FK_SALESORDER_id])    REFERENCES [dbo].[SALESORDER] ([id])   ON DELETE CASCADE
  )
 
------------------------------------------------------------------
-- Table 'danishcrown' . 'TRUCKDRIVER'
------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TRUCKDRIVER]'))

CREATE TABLE TRUCKDRIVER(
	ID				INT		      NOT NULL PRIMARY KEY IDENTITY(1,1),
	MOBILENUMBER	INT           NULL,
	NAME			NVARCHAR(255) NULL,
	FK_TRUCK_ID		INT       NOT NULL,
	FOREIGN KEY(FK_TRUCK_ID) REFERENCES [dbo].[truck] ([ID]) ON DELETE CASCADE  
	)

-- -----------------------------------------------------
-- Table `danishcrown`.`DOCK`
-- -----------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DOCK]'))
BEGIN 
CREATE TABLE DOCK(
	ID					INT IDENTITY(1,1) NOT NULL,
	DOCKSTATUS			NVARCHAR(10) NULL,
	PACKINGTYPE			NVARCHAR(10) NULL,
	)

	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'BOX')
	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'BOX')
	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'TUB')
	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'TUB')
	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'XTREE')
	INSERT INTO DOCK (DOCKSTATUS, PACKINGTYPE) VALUES ('IDLE', 'XTREE')
END

-- -----------------------------------------------------
-- Table `danishcrown`."ErrorLog`
-- -----------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ERRORLOG]'))
BEGIN 
CREATE TABLE ERRORLOG (
	ID					INT           IDENTITY(1,1) NOT NULL,
	LOGDATE			    DATETIME      NOT NULL DEFAULT GETDATE(),
	ERRORMESSAGE		NVARCHAR(100) NULL,
	)
END
 
IF OBJECT_ID ('CheckDelays','TR') IS NOT NULL
    DROP TRIGGER CheckDelays;
GO

CREATE TRIGGER CheckDelays ON [LOADINGORDER] 
after UPDATE, INSERT, DELETE
as    
declare
      @MINUTES integer, 
      @ID integer, 
      @DATECOMPLETEDLOADING  datetime, 
      @DATEEXPECTEDCOMPLETEDLOADING datetime 
    
BEGIN 
  SELECT @DATECOMPLETEDLOADING = DATECOMPLETEDLOADING , 
         @DATEEXPECTEDCOMPLETEDLOADING = DATEEXPECTEDCOMPLETEDLOADING,
         @ID = ID, 
         @minutes = ( datediff (MINUTE, @DATEEXPECTEDCOMPLETEDLOADING, @DATECOMPLETEDLOADING ) )
   FROM  inserted;
IF ( @minutes >=   15 )
begin
       if exists(SELECT * from inserted) and exists (SELECT * from deleted)
       begin 
      	 INSERT INTO ERRORLOG (ERRORMESSAGE)  
		 VALUES   (CONCAT ('UPDATE: LOADING ORDER: ' , @ID, ' DELAYED FOR ', @minutes, ' MINUTES')  ) 
       end

       If exists (Select * from inserted) and not exists(Select * from deleted)
       begin
         INSERT INTO ERRORLOG (ERRORMESSAGE)  
	            VALUES   (CONCAT ('INSERT: LOADING ORDER: ' , @ID, ' DELAYED FOR ', @minutes, ' MINUTES')  ) 
        end
end

If exists(select * from deleted) and not exists(Select * from inserted)
begin 
    SELECT    @ID = ID   
	    FROM  deleted;

	INSERT INTO ERRORLOG (ERRORMESSAGE)  
	VALUES   (CONCAT ('DELETE: LOADING ORDER: ' , @ID)  ) 
end
END

GO
 
IF OBJECT_ID('SP_DELETEDATA') IS NOT NULL
  drop procedure SP_DELETEDATA
go

create Procedure SP_DELETEDATA 
   @deleteFromDate datetime 
as
  set nocount on
  delete from LOADINGORDER where DATEARRIVED < @deleteFromDate
  delete from SALESORDER where LOADINGDATE   < @deleteFromDate
go 
 